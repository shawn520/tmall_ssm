package xin.shawn.tmall.mapper;

import xin.shawn.tmall.pojo.Category;
import xin.shawn.tmall.util.Page;

import java.util.List;

public interface CategoryMapper {
    List<Category> list(Page page);

    int total();

    void add(Category category);
    void delete(int id);
    Category get(int id);
    void update(Category category);
}
