package xin.shawn.tmall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xin.shawn.tmall.mapper.CategoryMapper;
import xin.shawn.tmall.pojo.Category;
import xin.shawn.tmall.service.CategoryService;
import xin.shawn.tmall.util.Page;

import java.util.List;
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    CategoryMapper categoryMapper;

    @Override
    public int total() {
        return categoryMapper.total();
    }
    @Override
    public List<Category> list(Page page){
        return categoryMapper.list(page);
    }
    @Override
    public void add(Category category){
        categoryMapper.add(category);
    }

    @Override
    public void delete(int id) {
        categoryMapper.delete(id);
    }

    @Override
    public Category get(int id) {
        return categoryMapper.get(id);
    }

    @Override
    public void update(Category category) {
        categoryMapper.update(category);
    }
}
